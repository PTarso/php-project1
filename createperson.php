<?php
  require_once "includes/header.php";
  
  if (!isset($_SESSION['username']))
    header("location: login.php?error=notlogin");

?>

<h1>Create Person</h1><br>
<form action="includes/newperson.php" method="POST">
  <label for="personname">Name: </label>
  <input type="text" name="personname" placeholder="Person name:" required><br><br>
  <label for="personage">Age: </label>
  <input type="text" name="personage" placeholder="Person age:" required><br><br>
  <label for="personsex">Sex: </label>
  <input type="text" name="personsex" placeholder="Person sex" required><br><br>
  <input type="submit" name="create" value="Create">
</form>

<?php require_once "includes/footer.php"; ?>
