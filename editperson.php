<?php

  require_once "includes/header.php";

  $pid = "";

  if (!isset($_SESSION['username']))
    header("location: login.php?error=notlogin");
else
{

  if (isset($_GET['id']))
    $pid = htmlspecialchars($_GET['id']);
  elseif (!isset($_GET['id']))
    header("location: index.php");
 ?>

<h1>Update Person</h1>
<form action="includes/updateperson.php" method="POST">
  <label for="newpersonname">Name: </label>
  <input type="text" name="newpersonname" placeholder="name" required><br>
  <label for="newpersonage">Age: </label>
  <input type="text" name="newpersonage" placeholder="Age" required><br>
  <label for="newpersonsex">Sex: </label>
  <input type="text" name="newpersonsex" placeholder="Sex" required><br>
  <input type="hidden" name="personid" value="<?=$pid?>" required>
  <input type="submit" name="update" value="Update">
</form>

 <?php require_once "includes/footer.php";} ?>
