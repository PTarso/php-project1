<?php

  // Opens a connection to the database
  function connectdb()
  {
      // Connection variables - To be on a different file on future updates
      $db = [
        "servername" => "localhost",
        "user" => "root",
        "pass" => "",
        "dbname" => "project2",
        "port" => 3306
      ];

      // Connect to database
      $dbconnect = new mysqli($db['servername'], $db['user'], $db['pass'], $db['dbname'], $db['port']);

      // Throws error if not connected to db
      if ($dbconnect -> connect_error)
      {
        die("Connection to database failed: " . $dbconnect -> connect_error);
      }

        return $dbconnect;
  }

  // List all $person inside the table people
  function listquery()
  {
    // Connect to DB
    $dbconnect = connectdb();
    // Array that will store everything inside people
    $people = [];

    // List fetch query
    // $result is a php 7 pointer
    $result = $dbconnect->query("SELECT * FROM people");

    if (!$result)
    {
      echo "Error trying to fetch from database: " . $dbconnect->error;
      $dbconnect->close();
      exit();
    }

    if ($result->num_rows <=0)
    {
      echo "There is no data on the table";
      $dbconnect->close();
      return false;
    }

    else
    {
      while ($person = $result->fetch_assoc())
      {
        $people[] = $person;
      }

      return $people;
    }

  }

  // Check administrator credentials at the database
  function dbcheckcredentials(string $adminUsername, string $adminPassword)
  {

    // Connect to DB
    $dbconnect = connectdb();

    //Search Query
    $result = $dbconnect->query("SELECT * FROM administrators WHERE username = '$adminUsername' AND password = '$adminPassword'");

    if ($result->num_rows <= 0)
      return false;

    if (isset($result) !== false)
      return true;

  }

  // Adds a new person to the database table "people"
  function addperson(string $personname, int $personage, string $personsex)
  {
    // Connect to DB
    $dbconnect = connectdb();

    // Search Query
    $result = $dbconnect->query("INSERT INTO people (name, age, sex) VALUES ('$personname', '$personage', '$personsex')");
  }

  // Updates a person entry on the table "people"
  function updateperson(string $newpersonname, int $newpersonage, string $newpersonsex, $personid)
  {
    // Connect to DB
    $dbconnect = connectdb();

    // Updates query
    $result = $dbconnect->query("UPDATE people SET name='$newpersonname', age=$newpersonage, sex='$newpersonsex' WHERE id='$personid'");
  }

  // Deletes a person on the "people table"
  function deleteperson($personid)
  {
    // Connect to DB
    $dbconnect = connectdb();

    // Delete query
    $result = $dbconnect->query("DELETE FROM people WHERE id=$personid");
  }

 ?>
