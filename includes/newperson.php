<?php
  session_start();
  
  if (!isset($_POST['create']) || !isset($_SESSION['username']))
    header("location: ../login.php?error=notlogin");
  else
  {
    require_once "dbhandler.php";

    // Get data from POST
    $person_name = htmlspecialchars($_POST['personname']);
    $person_age = htmlspecialchars($_POST['personage']);
    $person_sex = htmlspecialchars($_POST['personsex']);
    echo "$person_name, $person_age, $person_sex";
    // Add person
    addperson($person_name, $person_age, $person_sex);

    // Throws you back to index.php
    header("location: ../index.php?msg=created");
  }

   ?>
