<?php

  session_start();

  //require_once "functions.php";
  require_once "dbhandler.php";

  /* verifies if came from official login page
  if not, redirects to login page and shows an error */
   if (isset($_POST['submit']))
  {
    $adminUsername = htmlspecialchars($_POST['usrname']);
    $adminPassword = md5(htmlspecialchars($_POST['pass']));

    // Check credentials
    $user = dbcheckCredentials($adminUsername, $adminPassword);

    if ($user !== false)
    {
      $_SESSION['username'] = $adminUsername; // It`s not recommended to store $_SESSION['password']
      header("location: ../index.php");
    }
    else
      header("location: ../login.php?error=wronglogin");
  //  checkcredentials($adminUsername, $adminPassword);
  }

  // Redirects to login page for login if not logged in
  else
  {
    header("location: ../login.php?error=notlogin");
  }



?>
