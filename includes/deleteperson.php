<?php

  session_start();
  $pid = "";

  if (!isset($_SESSION['username']))
    header("location: login.php?error=notlogin");
  else
  {
    require_once "dbhandler.php";
    if (isset($_GET['id']))
      $pid = htmlspecialchars($_GET['id']);
    elseif (!isset($_GET['id']))
      header("location: ../index.php");

      deleteperson($pid);
      header("location: ../index.php?msg=deleted");
  }

 ?>
