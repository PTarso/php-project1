<?php

  session_start();

  // Redirects to login page if not logged in
  if (!isset($_POST['update']) || !isset($_SESSION['username']))
    header("location: ../login.php?error=notlogin");
  else
  {
    require_once "dbhandler.php";

    $personid=htmlspecialchars($_POST['personid']);
    $newpersonname = htmlspecialchars($_POST['newpersonname']);
    $newpersonage = htmlspecialchars($_POST['newpersonage']);
    $newpersonsex = htmlspecialchars($_POST['newpersonsex']);

    updateperson($newpersonname, $newpersonage, $newpersonsex, $personid);

    header("location: ../index.php?msg=updated");
  }
 ?>
