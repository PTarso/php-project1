<?php

  if ($_GET['operation'] == "logout")
  {
    session_start();
    session_unset();
    session_destroy();
    header("location: ../index.php?msg=logout");
  }

?>
