<?php

  session_start();

// Changes Login/Logout button depending if already logged in or not
  if (isset($_SESSION['username']))
  {
    $labelname = "Log out";
    $link = "includes/logout.php?operation=logout";
  }
  else
  {
    $labelname = "Log in";
    $link = "login.php";
  }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/includes/style.css">
    <title>Project</title>
</head>
<body align="center">

  <!--Navigation bar-->
  <nav>
    <table align="center">
      <th><a href="index.php">list</a></th>
      <th><a href="<?php echo $link; ?>"><?php echo $labelname; ?></a></th>
      <th><a href="createperson.php">Create Person</a></th>
    </table>
  </nav>
  <hr>
