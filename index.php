<?php

  require_once "includes/header.php";
  $adminstuff = false;
  $updatecolumn = "";
  $updatelink = "";
  $deletecolumn = "";
  $deletelink = "";

  // Confirms login only if actually logged in
  if (isset($_SESSION['username']))
  {
      echo "User: " . $_SESSION['username'] . "<br>";
      $adminstuff = true;
  }

  // Returns message if you chose to log out
  if (isset($_GET['msg']))
    if ($_GET['msg'] == "logout")
      echo "You`re Logged out.<br>";
    elseif ($_GET['msg'] == "created")
      echo "Person entry created with success!<br>";
    elseif ($_GET['msg'] == "updated")
      echo "Person entry updated with success!<br>";
    elseif ($_GET['msg'] == "deleted")
      echo "Person deleted with success!<br>";
?>

<h1 align="center">People List</h1><br>

<?php

  // Calls person list fetch query
  require_once "includes/dbhandler.php";

  $people = listquery();

?>
<table align="center" border="3px;" style="text-align: center;">
  <tr>
    <th>Id</th>
    <th>Name</th>
    <th>Age</th>
    <th>Sex</th>
  </tr>
<?php

  // Foreach for listing person in people table
  foreach ($people as $person){?>
    <?php
      $uid = $person['id'];
      $update="editperson.php";
      $delete="includes/deleteperson.php";
      if ($adminstuff !== false)
      {
        $updatelink = "<a href=" . $update . "?id=" . $uid . ">Update</a>";
        $updatecolumn = "<td>$updatelink</td>";
        $deletelink = "<a href=" . $delete . "?id=" . $uid . ">Delete</a>";
        $deletecolumn = "<td>$deletelink</td>";
      }
     ?>
<!--List Table-->

<tr>
  <td><?=$person['id']?></td>
  <td><?=$person['name']?></td>
  <td><?=$person['age']?></td>
  <td><?=$person['sex']?></td>
  <?=$updatecolumn?>
  <?=$deletecolumn?>
</tr>
<?php }; ?>
</table>

<?php
  require_once "includes/footer.php";
?>
