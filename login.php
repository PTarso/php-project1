<?php require_once "includes/header.php"; ?>

<h1>Login</h1>
<h3 style="color:red;">
<?php

  if (isset($_SESSION['username']))
    header("location: index.php");

  // Error message handling from GET
  if (isset($_GET['error']))
  {

    if ($_GET['error'] == "notlogin")
    {
      echo "Please log in first";
    }

    if ($_GET['error'] == "wronglogin")
    {
      echo "Wrong username and/or password";
    }

    else
    {
      echo "";
    }

  }

 ?>
</h3>
<!--Login Form-->
<form action="includes/check.php" method="POST">
  <label for="usrname">Username: </label>
  <input type="text" name="usrname" id="usr" placeholder="Username..." required><br><br>
  <label for="pass">Password: </label>
  <input type="password" name="pass" id="pwd" placeholder="Password..." required><br><br>
  <input type="submit" name="submit" id="btn" value="Log In">
</form>

<?php require_once "includes/footer.php"; ?>
